import { Component, OnInit } from '@angular/core';
import { DataTourismService } from '../service/data-tourism.service';
import { PageRoute } from 'nativescript-angular/router';
import { InterestPoint } from '../entity/interest-point';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from "rxjs/operators";

@Component({
  selector: 'ns-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css'],
  moduleId: module.id,
})
export class SingleComponent implements OnInit {
  interestPoint:any;
  constructor(private pageRoute:PageRoute, private service: DataTourismService) { }

  ngOnInit() {
    
    this.pageRoute.activatedRoute.pipe(
      switchMap(ActivatedRoute =>ActivatedRoute.queryParams)
    
    ).subscribe(
      (data) => this.interestPoint = data      
    );
}
}
