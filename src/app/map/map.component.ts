import { Component, OnInit } from '@angular/core';
import { DataTourismService } from '../service/data-tourism.service';
import { InterestPoint } from '../entity/interest-point';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  moduleId: module.id,
})
export class MapComponent implements OnInit {
 interestList: InterestPoint[] = [] ;
 list = []; 
  constructor(private service: DataTourismService, private router:RouterExtensions) { }


  goTo(ip:InterestPoint) {
    this.router.navigate(['/single'], {
      queryParams: ip
    });
  }

  ngOnInit() {
    
    this.service.findAll().subscribe((data) => {this.list = data.values;
    for (let i = 0; i < this.list.length; i++) {
      let ip:InterestPoint = {name:this.list[i].nom, tarification:this.list[i].tarifsenclair, address: this.list[i].adresse };
      this.interestList.push(ip);
      console.log(ip);
      
    }})
  }


}
