"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_tourism_service_1 = require("../service/data-tourism.service");
var router_1 = require("nativescript-angular/router");
var MapComponent = /** @class */ (function () {
    function MapComponent(service, router) {
        this.service = service;
        this.router = router;
        this.interestList = [];
        this.list = [];
    }
    MapComponent.prototype.goTo = function (ip) {
        this.router.navigate(['/single'], {
            queryParams: ip
        });
    };
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.findAll().subscribe(function (data) {
            _this.list = data.values;
            for (var i = 0; i < _this.list.length; i++) {
                var ip = { name: _this.list[i].nom, tarification: _this.list[i].tarifsenclair, address: _this.list[i].adresse };
                _this.interestList.push(ip);
                console.log(ip);
            }
        });
    };
    MapComponent = __decorate([
        core_1.Component({
            selector: 'ns-map',
            templateUrl: './map.component.html',
            styleUrls: ['./map.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [data_tourism_service_1.DataTourismService, router_1.RouterExtensions])
    ], MapComponent);
    return MapComponent;
}());
exports.MapComponent = MapComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1hcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsd0VBQXFFO0FBRXJFLHNEQUErRDtBQVEvRDtJQUdFLHNCQUFvQixPQUEyQixFQUFVLE1BQXVCO1FBQTVELFlBQU8sR0FBUCxPQUFPLENBQW9CO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBaUI7UUFGakYsaUJBQVksR0FBb0IsRUFBRSxDQUFFO1FBQ3BDLFNBQUksR0FBRyxFQUFFLENBQUM7SUFDMkUsQ0FBQztJQUdyRiwyQkFBSSxHQUFKLFVBQUssRUFBZ0I7UUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUNoQyxXQUFXLEVBQUUsRUFBRTtTQUNoQixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0JBQVEsR0FBUjtRQUFBLGlCQVNDO1FBUEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQU0sS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ3BFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDMUMsSUFBSSxFQUFFLEdBQWlCLEVBQUMsSUFBSSxFQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLFlBQVksRUFBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDeEgsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFbEIsQ0FBQztRQUFBLENBQUMsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQXJCVSxZQUFZO1FBTnhCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsc0JBQXNCO1lBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ2xDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQUk2Qix5Q0FBa0IsRUFBaUIseUJBQWdCO09BSHJFLFlBQVksQ0F3QnhCO0lBQUQsbUJBQUM7Q0FBQSxBQXhCRCxJQXdCQztBQXhCWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRhVG91cmlzbVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL2RhdGEtdG91cmlzbS5zZXJ2aWNlJztcbmltcG9ydCB7IEludGVyZXN0UG9pbnQgfSBmcm9tICcuLi9lbnRpdHkvaW50ZXJlc3QtcG9pbnQnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLW1hcCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9tYXAuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9tYXAuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBNYXBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuIGludGVyZXN0TGlzdDogSW50ZXJlc3RQb2ludFtdID0gW10gO1xuIGxpc3QgPSBbXTsgXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2VydmljZTogRGF0YVRvdXJpc21TZXJ2aWNlLCBwcml2YXRlIHJvdXRlcjpSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuXG4gIGdvVG8oaXA6SW50ZXJlc3RQb2ludCkge1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL3NpbmdsZSddLCB7XG4gICAgICBxdWVyeVBhcmFtczogaXBcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIFxuICAgIHRoaXMuc2VydmljZS5maW5kQWxsKCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7dGhpcy5saXN0ID0gZGF0YS52YWx1ZXM7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCBpcDpJbnRlcmVzdFBvaW50ID0ge25hbWU6dGhpcy5saXN0W2ldLm5vbSwgdGFyaWZpY2F0aW9uOnRoaXMubGlzdFtpXS50YXJpZnNlbmNsYWlyLCBhZGRyZXNzOiB0aGlzLmxpc3RbaV0uYWRyZXNzZSB9O1xuICAgICAgdGhpcy5pbnRlcmVzdExpc3QucHVzaChpcCk7XG4gICAgICBjb25zb2xlLmxvZyhpcCk7XG4gICAgICBcbiAgICB9fSlcbiAgfVxuXG5cbn1cbiJdfQ==